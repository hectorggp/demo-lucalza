namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class alert_alertas
    {
        [Key]
        public int alert_codigo { get; set; }

        [Column("alert_alertas")]
        [StringLength(100)]
        public string alert_alertas1 { get; set; }

        public int? alert_usr_asignado { get; set; }

        public int? alert_usr_solicitado { get; set; }

        public DateTime? alert_fecha_grabacion { get; set; }

        public DateTime? alert_fecha_modificacion { get; set; }

        public virtual usr_usuarios usr_usuarios { get; set; }
    }
}

namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class enc_grado_sae
    {
        [Key]
        public int grd_codigo { get; set; }

        [StringLength(30)]
        public string grd_descripcion { get; set; }
    }
}

namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class enc_encuesta_det
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public enc_encuesta_det()
        {
            enc_encuesta = new HashSet<enc_encuesta>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int enc_det_codigo { get; set; }

        public int enc_det_marca { get; set; }

        public int? enc_det_qrt { get; set; }

        public int? enc_det_gal { get; set; }

        public int? enc_det_garr { get; set; }

        public int? enc_det_cub { get; set; }

        public int? enc_det_ton { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? enc_det_qrt_price { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? enc_det_gal_price { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? enc_det_garr_price { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? enc_det_cub_price { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? enc_det_ton_price { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<enc_encuesta> enc_encuesta { get; set; }

        public virtual enc_marcas enc_marcas { get; set; }
    }
}

namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class enc_marcas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public enc_marcas()
        {
            enc_encuesta_det = new HashSet<enc_encuesta_det>();
        }

        [Key]
        public int enc_mrc_codigo { get; set; }

        [StringLength(30)]
        public string enc_mrc_marca { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<enc_encuesta_det> enc_encuesta_det { get; set; }
    }
}

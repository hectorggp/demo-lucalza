namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cia_companias
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cia_companias()
        {
            per_permisos = new HashSet<per_permisos>();
            usr_usuarios = new HashSet<usr_usuarios>();
        }

        [Key]
        public int cia_codigo { get; set; }

        [StringLength(50)]
        public string cia_descripcion { get; set; }

        [StringLength(50)]
        public string cia_razon_social { get; set; }

        public int? cia_grupo { get; set; }

        [StringLength(50)]
        public string cia_direccion { get; set; }

        [StringLength(50)]
        public string cia_logo { get; set; }

        [StringLength(15)]
        public string cia_telefono { get; set; }

        [StringLength(15)]
        public string cia_fax { get; set; }

        [StringLength(10)]
        public string cia_nit { get; set; }

        public int? cia_codpai { get; set; }

        [StringLength(50)]
        public string cia_database_ext { get; set; }

        [StringLength(50)]
        public string cia_database_cons { get; set; }

        public DateTime? cia_fecha_grabacion { get; set; }

        public DateTime? cia_fecha_modificacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<per_permisos> per_permisos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<usr_usuarios> usr_usuarios { get; set; }
    }
}

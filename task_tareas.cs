namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class task_tareas
    {
        [Key]
        public int task_codigo { get; set; }

        [StringLength(50)]
        public string task_tarea { get; set; }

        public string task_memo { get; set; }

        [StringLength(10)]
        public string task_porcentaje { get; set; }

        public int? task_usr_asignado { get; set; }

        public int? task_usr_solicitado { get; set; }

        public DateTime? task_fecha_solicitud { get; set; }

        public DateTime? task_fecha_finalizacion { get; set; }

        public DateTime? task_fecha_grabacion { get; set; }

        public DateTime? task_fecha_modificacion { get; set; }

        public virtual usr_usuarios usr_usuarios { get; set; }
    }
}

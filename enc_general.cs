namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class enc_general
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public enc_general()
        {
            enc_encuesta = new HashSet<enc_encuesta>();
        }

        [Key]
        public int enc_gen_codigo { get; set; }

        public int? enc_gen_apli_codigo { get; set; }

        public int? enc_gen_grd_codigo { get; set; }

        public int? enc_gen_vis_codigo { get; set; }

        public int? enc_gen_api_codigo { get; set; }

        public int? enc_gen_index { get; set; }

        public virtual enc_api enc_api { get; set; }

        public virtual enc_aplicacion enc_aplicacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<enc_encuesta> enc_encuesta { get; set; }
    }
}

namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class enc_encuesta
    {
        [Key]
        [Column(Order = 0)]
        public int enc_codigo { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int enc_gen_codigo { get; set; }

        [StringLength(50)]
        public string enc_cardcode { get; set; }

        public int? enc_cia_codigo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? enc_gps_lat { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? enc_gps_long { get; set; }

        public DateTime? enc_fecha_grabacion { get; set; }

        public virtual enc_encuesta_det enc_encuesta_det { get; set; }

        public virtual enc_general enc_general { get; set; }
    }
}

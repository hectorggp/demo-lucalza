namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class per_permisos
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int per_codigo { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int per_men_codigo { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int per_cia_codigo { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int per_rol_codigo { get; set; }

        public int? per_creacion { get; set; }

        public int? per_actualizacion { get; set; }

        public int? per_eliminacion { get; set; }

        public DateTime? per_fecha_grabacion { get; set; }

        public DateTime? per_fecha_modificacion { get; set; }

        public virtual cia_companias cia_companias { get; set; }

        public virtual men_menu men_menu { get; set; }

        public virtual rol_roles rol_roles { get; set; }
    }
}

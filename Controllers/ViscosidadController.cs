﻿using DemoLucalza.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace DemoLucalza.Controllers
{
    [EnableCors(origins: "https://casitas-gt.web.app", headers: "*", methods: "*")]
    public class ViscosidadController : ApiController
    {
        public static void setHeaders()
        {
            AplicacionController.setHeaders();
        }

        public IHttpActionResult Options(int npage = 1, int nitems = 10, int tsort = 1, string tsortdir = "asc", string search = "")
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        public IHttpActionResult GetAllViscosidads(int npage = 1, int nitems = 10, int tsort = 1, string tsortdir = "asc", string search = "")
        {
            if (search == null) search = "";
            String tsortv = "vis_codigo";
            switch (tsort)
            {
                case 1: tsortv = "vis_codigo"; break;
                case 2: tsortv = "vis_descripcion"; break;
            }

            IList<ViscosidadViewModel> Viscosidads = new List<ViscosidadViewModel>();

            int rows;
            using (var ctx = new BdLucalza())
            {
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelectAllViscosidads", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SEARCHCOD", SqlDbType.NVarChar).Value = search;
                    cmd.Parameters.Add("@SEARCH", SqlDbType.NVarChar).Value = "%" + search + "%";
                    cmd.Parameters.Add("@SORTCL", SqlDbType.NVarChar).Value = tsortv;
                    cmd.Parameters.Add("@SORTDR", SqlDbType.NVarChar).Value = tsortdir;
                    cmd.Parameters.Add("@SKIP", SqlDbType.Int).Value = ((npage - 1 ) * nitems);
                    cmd.Parameters.Add("@PAGESIZE", SqlDbType.Int).Value = nitems;
                    cmd.Parameters.Add("@CONT", SqlDbType.Int).Direction = ParameterDirection.Output;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Viscosidads.Add(new ViscosidadViewModel(reader.GetInt32(0), reader.GetString(1).Trim()));
                            }
                        }
                    }

                    rows = Convert.ToInt32(cmd.Parameters["@CONT"].Value);
                }
            }

            if (Viscosidads.Count == 0)
            {
                // return NotFound();
            }
            var page = new PageBag<ViscosidadViewModel> { items = Viscosidads, search = search, TotalRows = rows };
            var ret = Ok(page);
            setHeaders();
            return ret;
        }

        public IHttpActionResult PostNewViscosidad(ViscosidadViewModel Viscosidad)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            using (var ctx = new BdLucalza())
            {
                ctx.enc_viscosidad.Add(new enc_viscosidad()
                {
                    //vis_codigo = Viscosidad.Id,
                    vis_descripcion = Viscosidad.Descripcion
                });

                ctx.SaveChanges();
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }

        public IHttpActionResult Put(ViscosidadViewModel Viscosidad)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            using (var ctx = new BdLucalza())
            {
                var existingViscosidad = ctx.enc_viscosidad.Where(s => s.vis_codigo == Viscosidad.Id)
                                                        .FirstOrDefault<enc_viscosidad>();

                if (existingViscosidad != null)
                {
                    existingViscosidad.vis_descripcion = Viscosidad.Descripcion;

                    ctx.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.HttpOptions]
        public IHttpActionResult Options(int id)
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid Viscosidad id");

            using (var ctx = new BdLucalza())
            {
                var Viscosidad = ctx.enc_viscosidad
                    .Where(s => s.vis_codigo == id)
                    .FirstOrDefault();

                ctx.Entry(Viscosidad).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }

    }
}

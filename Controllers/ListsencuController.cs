﻿using DemoLucalza.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace DemoLucalza.Controllers
{ 
    [EnableCors(origins: "https://casitas-gt.web.app", headers: "*", methods: "*")]
    public class ListsencuController : ApiController
    { 
        public static void setHeaders()
        {
            AplicacionController.setHeaders();
        }

        [System.Web.Http.Route("api/enc_aplicacion")]
        public IHttpActionResult OptionsGradosaes()
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/enc_aplicacion")]
        public IHttpActionResult GetAllGradoSaes()
        {
            IList<GeneralViewModel> GradoSaes = new List<GeneralViewModel>();
            using (var ctx = new BdLucalza())
            {
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelAplicacion", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var obj = new GeneralViewModel(); // todo: generar view model que solo lleve estos datos
                                obj.codigo = (int)reader["enc_apli_codigo"];
                                obj.apli = ((string)reader["enc_apli_descripcion"]).Trim();
                                obj.index = (int)reader["enc_gen_index"];
                                GradoSaes.Add(obj);
                            }
                        }
                    }
                }
            }

            var ret = Ok(GradoSaes);
            setHeaders();
            return ret;
        }


        [System.Web.Http.Route("api/enc_gradosae")]
        public IHttpActionResult OptionsGradosaes(int apliCod)
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/enc_gradosae")]
        public IHttpActionResult GetAllGradoSaes(int apliCod)
        {
            IList<GradoSaeViewModel> GradoSaes = new List<GradoSaeViewModel>();
            using (var ctx = new BdLucalza())
            {
                //SelGradosae, @APLICOD
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelGradosae", con))
                { 
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@APLICOD", SqlDbType.Int).Value = apliCod;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                GradoSaes.Add(new GradoSaeViewModel(reader.GetInt32(0), reader.GetString(1).Trim()));
                            }
                        }
                    }
                }
            }

            var ret = Ok(GradoSaes);
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/enc_viscosidad")]
        public IHttpActionResult OptionsViscosidad(int apliCod, int gradosaeCod)
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/enc_viscosidad")]
        public IHttpActionResult GetAllViscosidad(int apliCod, int gradosaeCod)
        {
            IList<GradoSaeViewModel> Viscosidad = new List<GradoSaeViewModel>();
            using (var ctx = new BdLucalza())
            {
                //SelGradosae, @APLICOD
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelViscodad", con))
                { 
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@APLICOD", SqlDbType.Int).Value = apliCod;
                    cmd.Parameters.Add("@GRDCOD", SqlDbType.Int).Value = gradosaeCod;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Viscosidad.Add(new GradoSaeViewModel(reader.GetInt32(0), reader.GetString(1).Trim()));
                            }
                        }
                    }
                }
            }

            var ret = Ok(Viscosidad);
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/enc_api")]
        public IHttpActionResult OptionsApi(int apliCod, int gradosaeCod, int viscosidadCod)
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/enc_api")]
        public IHttpActionResult GetAllVApi(int apliCod, int gradosaeCod, int viscosidadCod)
        {
            IList<GradoSaeViewModel> Viscosidad = new List<GradoSaeViewModel>();
            using (var ctx = new BdLucalza())
            {
                //SelGradosae, @APLICOD
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelApi", con))
                { 
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@APLICOD", SqlDbType.Int).Value = apliCod;
                    cmd.Parameters.Add("@GRDCOD", SqlDbType.Int).Value = gradosaeCod;
                    cmd.Parameters.Add("@VSCOD", SqlDbType.Int).Value = viscosidadCod;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Viscosidad.Add(new GradoSaeViewModel(reader.GetInt32(0), reader.GetString(1).Trim()));
                            }
                        }
                    }
                }
            }

            var ret = Ok(Viscosidad);
            setHeaders();
            return ret;
        }
    }
}

﻿using DemoLucalza.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace DemoLucalza.Controllers
{
    [EnableCors(origins: "https://casitas-gt.web.app", headers: "*", methods: "*")]
    public class ApiiController : ApiController
    {
        public static void setHeaders()
        {
            AplicacionController.setHeaders();
        }

        public IHttpActionResult Options(int npage = 1, int nitems = 10, int tsort = 1, string tsortdir = "asc", string search = "")
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        public IHttpActionResult GetAllApiis(int npage = 1, int nitems = 10, int tsort = 1, string tsortdir = "asc", string search = "")
        {
            if (search == null) search = "";
            String tsortv = "api_codigo";
            switch (tsort)
            {
                case 1: tsortv = "api_codigo"; break;
                case 2: tsortv = "api_descripcion"; break;
            }

            IList<ApiiViewModel> Apiis = new List<ApiiViewModel>();

            int rows;
            using (var ctx = new BdLucalza())
            {
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelectAllApiis", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SEARCHCOD", SqlDbType.NVarChar).Value = search;
                    cmd.Parameters.Add("@SEARCH", SqlDbType.NVarChar).Value = "%" + search + "%";
                    cmd.Parameters.Add("@SORTCL", SqlDbType.NVarChar).Value = tsortv;
                    cmd.Parameters.Add("@SORTDR", SqlDbType.NVarChar).Value = tsortdir;
                    cmd.Parameters.Add("@SKIP", SqlDbType.Int).Value = ((npage - 1 ) * nitems);
                    cmd.Parameters.Add("@PAGESIZE", SqlDbType.Int).Value = nitems;
                    cmd.Parameters.Add("@CONT", SqlDbType.Int).Direction = ParameterDirection.Output;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Apiis.Add(new ApiiViewModel(reader.GetInt32(0), Encoding.ASCII.GetString((byte[])reader["api_descripcion"]).Trim()));
                            }
                        }
                    }

                    rows = Convert.ToInt32(cmd.Parameters["@CONT"].Value);
                }
            }

            if (Apiis.Count == 0)
            {
                // return NotFound();
            }
            var page = new PageBag<ApiiViewModel> { items = Apiis, search = search, TotalRows = rows };
            var ret = Ok(page);
            setHeaders();
            return ret;
        }

        public IHttpActionResult PostNewApii(ApiiViewModel Apii)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            using (var ctx = new BdLucalza())
            {
                ctx.enc_api.Add(new enc_api()
                {
                    //api_codigo = Apii.Id,
                    api_descripcion = Encoding.ASCII.GetBytes(Apii.Descripcion)
                });

                ctx.SaveChanges();
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }

        public IHttpActionResult Put(ApiiViewModel Apii)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            using (var ctx = new BdLucalza())
            {
                var existingApii = ctx.enc_api.Where(s => s.api_codigo == Apii.Id)
                                                        .FirstOrDefault<enc_api>();

                if (existingApii != null)
                {
                    existingApii.api_descripcion = Encoding.ASCII.GetBytes(Apii.Descripcion);

                    ctx.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.HttpOptions]
        public IHttpActionResult Options(int id)
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid Apii id");

            using (var ctx = new BdLucalza())
            {
                var Apii = ctx.enc_api
                    .Where(s => s.api_codigo == id)
                    .FirstOrDefault();

                ctx.Entry(Apii).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }

    }
}

﻿using DemoLucalza.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace DemoLucalza.Controllers
{
    [EnableCors(origins: "https://casitas-gt.web.app", headers: "*", methods: "*")]
    public class ListasController : ApiController
    {
        private void setHeaders()
        {
            AplicacionController.setHeaders();
        }

        [System.Web.Http.Route("api/aplicaciones")]
        public IHttpActionResult OptionsAplicacions()
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/aplicaciones")]
        public IHttpActionResult GetAplicacions()
        {
            IList<AplicacionViewModel> Aplicacions = new List<AplicacionViewModel>();

            using (var ctx = new BdLucalza())
            {
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelectAplicacion", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Aplicacions.Add(new AplicacionViewModel(reader.GetInt32(0), reader.GetString(1).Trim()));
                            }
                        }
                    }

                }
            }

            var ret = Ok(Aplicacions);
            setHeaders();
            return ret;
        }


        [System.Web.Http.Route("api/gradossae")]
        public IHttpActionResult OptionsGradosaes()
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/gradossae")]
        public IHttpActionResult GetGradosaes()
        {
            IList<GradoSaeViewModel> Gradosaes = new List<GradoSaeViewModel>();

            using (var ctx = new BdLucalza())
            {
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelectGradosae", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Gradosaes.Add(new GradoSaeViewModel(reader.GetInt32(0), reader.GetString(1).Trim()));
                            }
                        }
                    }

                }
            }

            var ret = Ok(Gradosaes);
            setHeaders();
            return ret;
        }


        [System.Web.Http.Route("api/viscosidades")]
        public IHttpActionResult OptionsViscosidad()
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/viscosidades")]
        public IHttpActionResult GetViscosidad()
        {
            IList<ViscosidadViewModel> Viscosidad = new List<ViscosidadViewModel>();

            using (var ctx = new BdLucalza())
            {
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelectViscosidad", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Viscosidad.Add(new ViscosidadViewModel(reader.GetInt32(0), reader.GetString(1).Trim()));
                            }
                        }
                    }

                }
            }

            var ret = Ok(Viscosidad);
            setHeaders();
            return ret;
        }


        [System.Web.Http.Route("api/apis")]
        public IHttpActionResult OptionsApii()
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/apis")]
        public IHttpActionResult GetApii()
        {
            IList<ApiiViewModel> Apii = new List<ApiiViewModel>();

            using (var ctx = new BdLucalza())
            {
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelectApii", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Apii.Add(new ApiiViewModel(reader.GetInt32(0), Encoding.ASCII.GetString((byte[])reader["api_descripcion"]).Trim()));
                            }
                        }
                    }

                }
            }

            var ret = Ok(Apii);
            setHeaders();
            return ret;
        }


        [System.Web.Http.Route("api/marcas")]
        public IHttpActionResult OptionsMarca()
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.Route("api/marcas")]
        public IHttpActionResult GetMarca()
        {
            IList<ApiiViewModel> Apii = new List<ApiiViewModel>();

            using (var ctx = new BdLucalza())
            {
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelectMarca", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Apii.Add(new ApiiViewModel(reader.GetInt32(0), reader.GetString(1).Trim()));
                            }
                        }
                    }

                }
            }

            var ret = Ok(Apii);
            setHeaders();
            return ret;
        }

    }
}

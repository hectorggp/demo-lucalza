﻿using DemoLucalza.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace DemoLucalza.Controllers
{
    [EnableCors(origins: "https://casitas-gt.web.app", headers: "*", methods: "*")]
    public class GeneralController : ApiController
    {
        private void setHeaders()
        {
            AplicacionController.setHeaders();
        }

        public IHttpActionResult Options(int npage = 1, int nitems = 10, int tsort = 1, string tsortdir = "asc", string search = "")
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        public IHttpActionResult GetAllGenerals(int npage = 1, int nitems = 10, int tsort = 1, string tsortdir = "asc", string search = "")
        {
            if (search == null) search = "";
            String tsortv = "enc_gen_codigo";
            switch(tsort)
            {
                case 2: tsortv = "enc_apli_descripcion"; break;
                case 3: tsortv = "grd_descripcion"; break;
                case 4: tsortv = "vis_descripcion"; break;
                case 5: tsortv = "api_descripcion"; break;
                case 6: tsortv = "enc_gen_index"; break;
            }

            IList<GeneralViewModel> Generals = new List<GeneralViewModel>();

            int rows;
            using (var ctx = new BdLucalza())
            {
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelectAllEncgenerals", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SEARCHCOD", SqlDbType.NVarChar).Value = search;
                    cmd.Parameters.Add("@SEARCH", SqlDbType.NVarChar).Value = "%" + search + "%";
                    cmd.Parameters.Add("@SORTCL", SqlDbType.NVarChar).Value = tsortv;
                    cmd.Parameters.Add("@SORTDR", SqlDbType.NVarChar).Value = tsortdir;
                    cmd.Parameters.Add("@SKIP", SqlDbType.Int).Value = ((npage - 1 ) * nitems);
                    cmd.Parameters.Add("@PAGESIZE", SqlDbType.Int).Value = nitems;
                    cmd.Parameters.Add("@CONT", SqlDbType.Int).Direction = ParameterDirection.Output;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                GeneralViewModel g = new GeneralViewModel();
                                g.codigo = (int) reader["enc_gen_codigo"];
                                g.index = (int)reader["enc_gen_index"];
                                g.apliCodigo = (int) reader["enc_gen_apli_codigo"];
                                g.grdCodigo = (int) reader["enc_gen_grd_codigo"];
                                g.visCodigo = (int) reader["enc_gen_vis_codigo"];
                                g.apiCodigo = (int) reader["enc_gen_api_codigo"];
                                g.apli = ((string) reader["enc_apli_descripcion"]).Trim();
                                g.grd = ((string) reader["grd_descripcion"]).Trim();
                                g.vis = ((string) reader["vis_descripcion"]).Trim();
                                g.api = (Encoding.ASCII.GetString((byte[])reader["api_descripcion"])).Trim();
                                Generals.Add(g);
                            }
                        }
                    }

                    rows = Convert.ToInt32(cmd.Parameters["@CONT"].Value);
                }
            }

            var page = new PageBag<GeneralViewModel> { items = Generals, search = search, TotalRows = rows };
            var ret = Ok(page);
            setHeaders();
            return ret;
        }
        
        public IHttpActionResult PostNewGeneral(GeneralViewModel General)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            using (var ctx = new BdLucalza())
            {
                ctx.enc_general.Add(new enc_general()
                {
                    enc_gen_index = General.index,
                    enc_gen_apli_codigo = General.apliCodigo,
                    enc_gen_grd_codigo = General.grdCodigo,
                    enc_gen_vis_codigo = General.visCodigo,
                    enc_gen_api_codigo = General.apiCodigo
                });

                ctx.SaveChanges();
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }
        
        public IHttpActionResult Put(GeneralViewModel General)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            using (var ctx = new BdLucalza())
            {
                var existingGeneral = ctx.enc_general.Where(s => s.enc_gen_codigo == General.codigo)
                                                        .FirstOrDefault<enc_general>();

                if (existingGeneral != null)
                {
                    existingGeneral.enc_gen_apli_codigo = General.apliCodigo;
                    existingGeneral.enc_gen_grd_codigo = General.grdCodigo;
                    existingGeneral.enc_gen_vis_codigo = General.visCodigo;
                    existingGeneral.enc_gen_api_codigo = General.apiCodigo;

                    ctx.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.HttpOptions]
        public IHttpActionResult Options(int id)
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid General id");

            using (var ctx = new BdLucalza())
            {
                var General = ctx.enc_general
                    .Where(s => s.enc_gen_codigo == id)
                    .FirstOrDefault();

                ctx.Entry(General).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }
    }
}

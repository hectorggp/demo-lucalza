﻿using DemoLucalza.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace DemoLucalza.Controllers
{
    [EnableCors(origins: "https://casitas-gt.web.app", headers: "*", methods: "*")]
    public class AplicacionController : ApiController
    {
        public static void setHeaders()
        {
            HttpContext.Current.Response.Headers.Add("Access-Control-Allow-Origin", "https://casitas-gt.web.app");
            HttpContext.Current.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type,Access-Control-Allow-Origin");
            HttpContext.Current.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
        }

        public IHttpActionResult Options(int npage = 1, int nitems = 10, int tsort = 1, string tsortdir = "asc", string search = "")
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        public IHttpActionResult GetAllAplicacions(int npage = 1, int nitems = 10, int tsort = 1, string tsortdir = "asc", string search = "")
        {
            if (search == null) search = "";
            String tsortv = "enc_apli_codigo";
            switch(tsort)
            {
                case 1: tsortv = "enc_apli_codigo"; break;
                case 2: tsortv = "enc_apli_descripcion"; break;
            }

            IList<AplicacionViewModel> Aplicacions = new List<AplicacionViewModel>();

            int rows;
            using (var ctx = new BdLucalza())
            {
                var con = ctx.Database.Connection as SqlConnection;
                using (SqlCommand cmd = new SqlCommand("SelectAllAplicacions", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SEARCHCOD", SqlDbType.NVarChar).Value = search;
                    cmd.Parameters.Add("@SEARCH", SqlDbType.NVarChar).Value = "%" + search + "%";
                    cmd.Parameters.Add("@SORTCL", SqlDbType.NVarChar).Value = tsortv;
                    cmd.Parameters.Add("@SORTDR", SqlDbType.NVarChar).Value = tsortdir;
                    cmd.Parameters.Add("@SKIP", SqlDbType.Int).Value = ((npage - 1 ) * nitems);
                    cmd.Parameters.Add("@PAGESIZE", SqlDbType.Int).Value = nitems;
                    cmd.Parameters.Add("@CONT", SqlDbType.Int).Direction = ParameterDirection.Output;
                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Aplicacions.Add(new AplicacionViewModel(reader.GetInt32(0), reader.GetString(1).Trim()));
                            }
                        }
                    }

                    rows = Convert.ToInt32(cmd.Parameters["@CONT"].Value);
                }
            }

            if (Aplicacions.Count == 0)
            {
                // return NotFound();
            }
            var page = new PageBag<AplicacionViewModel> { items = Aplicacions, search = search, TotalRows = rows };
            var ret = Ok(page);
            setHeaders();
            return ret;
        }

        public IHttpActionResult PostNewAplicacion(AplicacionViewModel Aplicacion)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            using (var ctx = new BdLucalza())
            {
                ctx.enc_aplicacion.Add(new enc_aplicacion()
                {
                    //enc_apli_codigo = Aplicacion.Id,
                    enc_apli_descripcion = Aplicacion.Descripcion
                });

                ctx.SaveChanges();
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }

        public IHttpActionResult Put(AplicacionViewModel Aplicacion)
        {
            if (!ModelState.IsValid)
                return BadRequest("Not a valid model");

            using (var ctx = new BdLucalza())
            {
                var existingAplicacion = ctx.enc_aplicacion.Where(s => s.enc_apli_codigo == Aplicacion.Id)
                                                        .FirstOrDefault<enc_aplicacion>();

                if (existingAplicacion != null)
                {
                    existingAplicacion.enc_apli_descripcion = Aplicacion.Descripcion;

                    ctx.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.HttpOptions]
        public IHttpActionResult Options(int id)
        {
            var ret = Ok();
            setHeaders();
            return ret;
        }

        [System.Web.Http.HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid Aplicacion id");

            using (var ctx = new BdLucalza())
            {
                var Aplicacion = ctx.enc_aplicacion
                    .Where(s => s.enc_apli_codigo == id)
                    .FirstOrDefault();

                ctx.Entry(Aplicacion).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();
            }

            var ret = Ok();
            setHeaders();
            return ret;
        }

    }
}

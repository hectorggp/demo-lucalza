namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class men_menu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public men_menu()
        {
            per_permisos = new HashSet<per_permisos>();
        }

        [Key]
        public int men_codigo { get; set; }

        public int? men_orden { get; set; }

        public int? men_nivel_superior { get; set; }

        public int? men_nivel { get; set; }

        [StringLength(10)]
        public string men_icono { get; set; }

        [StringLength(50)]
        public string men_nombre { get; set; }

        public DateTime? men_fecha_grabacion { get; set; }

        public DateTime? men_fecha_modificacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<per_permisos> per_permisos { get; set; }
    }
}

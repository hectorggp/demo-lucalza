namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class usr_usuarios
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public usr_usuarios()
        {
            alert_alertas = new HashSet<alert_alertas>();
            mens_mensajes = new HashSet<mens_mensajes>();
            task_tareas = new HashSet<task_tareas>();
            cia_companias = new HashSet<cia_companias>();
        }

        [Key]
        public int usr_codigo { get; set; }

        [StringLength(20)]
        public string usr_usuario { get; set; }

        [StringLength(40)]
        public string usr_nombre { get; set; }

        [StringLength(40)]
        public string usr_apellidos { get; set; }

        [StringLength(20)]
        public string usr_departamento { get; set; }

        public int usr_cia_codigo { get; set; }

        [StringLength(40)]
        public string usr_password { get; set; }

        [StringLength(50)]
        public string usr_imagen { get; set; }

        public int? usr_rol_codigo { get; set; }

        public int? usr_root { get; set; }

        public DateTime? usr_fecha_grabacion { get; set; }

        public DateTime? usr_fecha_modificacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<alert_alertas> alert_alertas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mens_mensajes> mens_mensajes { get; set; }

        public virtual rol_roles rol_roles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<task_tareas> task_tareas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cia_companias> cia_companias { get; set; }
    }
}

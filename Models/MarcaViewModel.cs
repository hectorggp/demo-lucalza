﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoLucalza.Models
{
    public class MarcaViewModel : AplicacionViewModel
    {
        public MarcaViewModel() : base() { }
        public MarcaViewModel(int Id, string Descripcion) : base(Id, Descripcion) { }
    }

}
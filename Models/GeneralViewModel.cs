﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoLucalza.Models
{
    public class GeneralViewModel
    {
        public int codigo { get; set; }
        public int apliCodigo { get; set; }
        public int grdCodigo { get; set; }
        public int visCodigo { get; set; }
        public int apiCodigo { get; set; }
        public int index { get; set; }
        public string apli {get; set; }
        public string grd {get; set; }
        public string vis {get; set; }
        public string api {get; set; }
    }
}
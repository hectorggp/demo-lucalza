﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoLucalza.Models
{
    public class PageBag<T>
    {
        public int TotalRows { get; set; }
        public string search { get; set; }
        public IList<T> items { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoLucalza.Models
{
    public class GradoSaeViewModel : AplicacionViewModel
    {
        public GradoSaeViewModel() : base() { }
        public GradoSaeViewModel(int Id, string Descripcion) : base(Id, Descripcion) { }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoLucalza.Models
{
    public class AplicacionViewModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public AplicacionViewModel() { }
        public AplicacionViewModel(int Id, string Descripcion)
        {
            this.Id = Id;
            this.Descripcion = Descripcion;
        }
    }
}
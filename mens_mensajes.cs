namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class mens_mensajes
    {
        [Key]
        public int mens_codigo { get; set; }

        [StringLength(50)]
        public string mens_mensaje { get; set; }

        public int? mens_usr_asignado { get; set; }

        public int? mens_usr_solicitado { get; set; }

        public DateTime? mens_fecha_grabacion { get; set; }

        public DateTime? mens_fecha_modificaion { get; set; }

        public virtual usr_usuarios usr_usuarios { get; set; }
    }
}

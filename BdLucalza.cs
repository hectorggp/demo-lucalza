namespace DemoLucalza
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BdLucalza : DbContext
    {
        public BdLucalza()
            : base("name=BdLucalza")
        {
        }

        public virtual DbSet<alert_alertas> alert_alertas { get; set; }
        public virtual DbSet<cia_companias> cia_companias { get; set; }
        public virtual DbSet<enc_api> enc_api { get; set; }
        public virtual DbSet<enc_aplicacion> enc_aplicacion { get; set; }
        public virtual DbSet<enc_encuesta> enc_encuesta { get; set; }
        public virtual DbSet<enc_encuesta_det> enc_encuesta_det { get; set; }
        public virtual DbSet<enc_general> enc_general { get; set; }
        public virtual DbSet<enc_grado_sae> enc_grado_sae { get; set; }
        public virtual DbSet<enc_marcas> enc_marcas { get; set; }
        public virtual DbSet<enc_viscosidad> enc_viscosidad { get; set; }
        public virtual DbSet<men_menu> men_menu { get; set; }
        public virtual DbSet<mens_mensajes> mens_mensajes { get; set; }
        public virtual DbSet<per_permisos> per_permisos { get; set; }
        public virtual DbSet<rol_roles> rol_roles { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<task_tareas> task_tareas { get; set; }
        public virtual DbSet<usr_usuarios> usr_usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<alert_alertas>()
                .Property(e => e.alert_alertas1)
                .IsFixedLength();

            modelBuilder.Entity<cia_companias>()
                .Property(e => e.cia_telefono)
                .IsFixedLength();

            modelBuilder.Entity<cia_companias>()
                .Property(e => e.cia_fax)
                .IsFixedLength();

            modelBuilder.Entity<cia_companias>()
                .Property(e => e.cia_nit)
                .IsFixedLength();

            modelBuilder.Entity<cia_companias>()
                .HasMany(e => e.per_permisos)
                .WithRequired(e => e.cia_companias)
                .HasForeignKey(e => e.per_cia_codigo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cia_companias>()
                .HasMany(e => e.usr_usuarios)
                .WithMany(e => e.cia_companias)
                .Map(m => m.ToTable("emp_empresas").MapLeftKey("emp_cia_codigo").MapRightKey("emp_usr_codigo"));

            modelBuilder.Entity<enc_api>()
                .HasMany(e => e.enc_general)
                .WithOptional(e => e.enc_api)
                .HasForeignKey(e => e.enc_gen_api_codigo);

            modelBuilder.Entity<enc_aplicacion>()
                .Property(e => e.enc_apli_descripcion)
                .IsFixedLength();

            modelBuilder.Entity<enc_aplicacion>()
                .HasMany(e => e.enc_general)
                .WithOptional(e => e.enc_aplicacion)
                .HasForeignKey(e => e.enc_gen_apli_codigo);

            modelBuilder.Entity<enc_encuesta>()
                .Property(e => e.enc_cardcode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<enc_encuesta>()
                .Property(e => e.enc_gps_lat)
                .HasPrecision(18, 0);

            modelBuilder.Entity<enc_encuesta>()
                .Property(e => e.enc_gps_long)
                .HasPrecision(18, 0);

            modelBuilder.Entity<enc_encuesta_det>()
                .Property(e => e.enc_det_qrt_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<enc_encuesta_det>()
                .Property(e => e.enc_det_gal_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<enc_encuesta_det>()
                .Property(e => e.enc_det_garr_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<enc_encuesta_det>()
                .Property(e => e.enc_det_cub_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<enc_encuesta_det>()
                .Property(e => e.enc_det_ton_price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<enc_encuesta_det>()
                .HasMany(e => e.enc_encuesta)
                .WithRequired(e => e.enc_encuesta_det)
                .HasForeignKey(e => e.enc_codigo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<enc_general>()
                .HasMany(e => e.enc_encuesta)
                .WithRequired(e => e.enc_general)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<enc_grado_sae>()
                .Property(e => e.grd_descripcion)
                .IsFixedLength();

            modelBuilder.Entity<enc_marcas>()
                .Property(e => e.enc_mrc_marca)
                .IsFixedLength();

            modelBuilder.Entity<enc_marcas>()
                .HasMany(e => e.enc_encuesta_det)
                .WithRequired(e => e.enc_marcas)
                .HasForeignKey(e => e.enc_det_marca)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<enc_viscosidad>()
                .Property(e => e.vis_descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<men_menu>()
                .Property(e => e.men_icono)
                .IsFixedLength();

            modelBuilder.Entity<men_menu>()
                .Property(e => e.men_nombre)
                .IsUnicode(false);

            modelBuilder.Entity<men_menu>()
                .HasMany(e => e.per_permisos)
                .WithRequired(e => e.men_menu)
                .HasForeignKey(e => e.per_men_codigo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<mens_mensajes>()
                .Property(e => e.mens_mensaje)
                .IsFixedLength();

            modelBuilder.Entity<rol_roles>()
                .Property(e => e.rol_descripcion)
                .IsFixedLength();

            modelBuilder.Entity<rol_roles>()
                .HasMany(e => e.per_permisos)
                .WithRequired(e => e.rol_roles)
                .HasForeignKey(e => e.per_rol_codigo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<rol_roles>()
                .HasMany(e => e.usr_usuarios)
                .WithOptional(e => e.rol_roles)
                .HasForeignKey(e => e.usr_rol_codigo);

            modelBuilder.Entity<task_tareas>()
                .Property(e => e.task_tarea)
                .IsFixedLength();

            modelBuilder.Entity<task_tareas>()
                .Property(e => e.task_porcentaje)
                .IsFixedLength();

            modelBuilder.Entity<usr_usuarios>()
                .Property(e => e.usr_usuario)
                .IsFixedLength();

            modelBuilder.Entity<usr_usuarios>()
                .Property(e => e.usr_nombre)
                .IsFixedLength();

            modelBuilder.Entity<usr_usuarios>()
                .Property(e => e.usr_apellidos)
                .IsFixedLength();

            modelBuilder.Entity<usr_usuarios>()
                .Property(e => e.usr_departamento)
                .IsFixedLength();

            modelBuilder.Entity<usr_usuarios>()
                .Property(e => e.usr_password)
                .IsFixedLength();

            modelBuilder.Entity<usr_usuarios>()
                .Property(e => e.usr_imagen)
                .IsFixedLength();

            modelBuilder.Entity<usr_usuarios>()
                .HasMany(e => e.alert_alertas)
                .WithOptional(e => e.usr_usuarios)
                .HasForeignKey(e => e.alert_usr_asignado);

            modelBuilder.Entity<usr_usuarios>()
                .HasMany(e => e.mens_mensajes)
                .WithOptional(e => e.usr_usuarios)
                .HasForeignKey(e => e.mens_usr_asignado);

            modelBuilder.Entity<usr_usuarios>()
                .HasMany(e => e.task_tareas)
                .WithOptional(e => e.usr_usuarios)
                .HasForeignKey(e => e.task_usr_asignado);
        }
    }
}

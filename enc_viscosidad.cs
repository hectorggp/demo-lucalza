namespace DemoLucalza
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class enc_viscosidad
    {
        [Key]
        public int vis_codigo { get; set; }

        [StringLength(30)]
        public string vis_descripcion { get; set; }
    }
}
